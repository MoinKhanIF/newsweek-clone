# Newsweek Clone

[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![LinkedIn][linkedin-shield]][linkedin-url]


Recreating a version of [newsweek.com](https://www.newsweek.com/) website.

Final live version : [Moins Newsweek](https://raw.githack.com/moinkhanif/newsweek-clone/bootstrap-v/index.html)

## About the project

![Newsweek](https://i.imgur.com/1HDZpCD.png)

We were to recreate Newsweek website. The above is my version of the website

## Features

* Made using Bootstrap

* Used minimal to no javascript

* Top Asides are sticky untill their content is further scrolled.

* Responsive in nature

* Used fontawesome for icons

<!-- MARKDOWN LINKS & IMAGES -->

[contributors-shield]: https://img.shields.io/github/contributors/moinkhanif/newsweek-clone.svg?style=flat-square
[contributors-url]: https://github.com/moinkhanif/newsweek-clone/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/moinkhanif/newsweek-clone.svg?style=flat-square
[forks-url]: https://github.com/moinkhanif/newsweek-clone/network/members
[stars-shield]: https://img.shields.io/github/stars/moinkhanif/newsweek-clone.svg?style=flat-square
[stars-url]: https://github.com/moinkhanif/newsweek-clone/stargazers
[issues-shield]: https://img.shields.io/github/issues/moinkhanif/newsweek-clone.svg?style=flat-square
[issues-url]: https://github.com/moinkhanif/newsweek-clone/issues
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/moinkhanif
[product-screenshot]: images/screenshot.png
